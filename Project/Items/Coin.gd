extends Node2D



var taken = false


func _on_Area2D_body_entered(body):
	if taken == false:
		$AnimationPlayer.play("Coin")
		taken=true
		get_tree().call_group("coin_selected", "more_coin")
