extends Node2D

var score
var player_before


func _ready():
	add_to_group("gameMaster")
	score = 0
	player_before = $Player.position.y
	#$Objects._creating_level()

func _process(_delta):
	
	if Input.is_action_just_pressed("menu"):
		get_tree().change_scene("res://Menu/GameOver.tscn")
	
	if($Player.position.y < player_before):
		player_before = $Player.position.y
		score = score +1
	$GUI/Control/Banner/HBoxContainer/Score.text = str(score)

	
	
func _on_Player_playerposition(position, camera_bottom_line):
	if(position > camera_bottom_line):
		get_tree().change_scene("res://Menu/GameOver.tscn")

func end_Game():
	get_tree().change_scene("res://Menu/GameOver.tscn")
