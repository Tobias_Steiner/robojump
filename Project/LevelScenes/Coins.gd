extends TileMap




onready var plattforms= get_node("../Objects")
onready var hazards= get_node("../Hazards")
onready var player= get_node("../Player")
var used_plattform_cells: Array
var _next_start

func _get_random(nums):
	randomize()
	var idx = randi() % nums
	return idx


func update_used_cells():
	var deleted_cells : Array
	used_plattform_cells = plattforms.get_used_cells()
	for cell in used_plattform_cells:
		var cell_world =map_to_world(cell)
		if cell_world.y > player.position.y:
			deleted_cells.append(cell)
	for cell in deleted_cells:
		used_plattform_cells.erase(cell)


func check_occupied_pos(cell) -> bool:
	var hazards_cells = hazards.get_used_cells()
	for hazard in hazards_cells:
		if cell.y == hazard.y && cell.x == hazard.x :
			return false
	return true

func _on_Objects_spawn_coins():
	update_used_cells()
	for i in 15:
		var cell = used_plattform_cells[_get_random(used_plattform_cells.size())]
		if check_occupied_pos(cell):
			var coin = load("res://Items/Coin.tscn").instance()
			var cell_world = map_to_world(cell)
			add_child(coin)
			coin.position.x= cell_world.x
			coin.position.y = cell_world.y -64
		
		
	
	
	
