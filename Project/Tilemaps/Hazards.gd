extends TileMap


var cell
onready var plattforms= get_node("../Objects")
onready var player= get_node("../Player")
var used_plattform_cells : Array
var cloud_list : Array
var _next_start

func _ready():
	add_to_group("delete_tiles_out_of_sight")
	add_to_group("danger")

func _on_Objects_create_hazards(next_start):
	_next_start = next_start
	create_hazards()
	delete_obsolete_tiles()
	



func delete_tiles(bottomline): #Funktion wird von der Camera aufgerufen
	var bottomline_cell = world_to_map(Vector2(0,bottomline))
	for cell in get_used_cells():
		if cell.y > bottomline_cell.y:
			set_cell(cell.x, cell.y, -1)
	if cloud_list.size() != 0:
		for cloud in cloud_list:
			if cloud.position.y > bottomline_cell.y:
				cloud.queue_free()
				cloud_list.erase(cloud)

func _get_random(nums):
	randomize()
	var idx = randi() % nums
	return idx

func update_used_cells():
	var deleted_cells : Array
	used_plattform_cells = plattforms.get_used_cells()
	for cell in used_plattform_cells:
		var cell_world =map_to_world(cell)
		if cell_world.y > player.position.y:
			deleted_cells.append(cell)
	for cell in deleted_cells:
		used_plattform_cells.erase(cell)

func create_hazards():
	update_used_cells()
	for i in 4:
		cell = used_plattform_cells[_get_random(used_plattform_cells.size())]
		var cell_w = map_to_world(cell)
		if cell_w.y < _next_start:
			used_plattform_cells.erase(cell)
			set_cell(cell.x,cell.y-1, 0)
	
	for i in 2:
		cell = used_plattform_cells[_get_random(used_plattform_cells.size())]
		used_plattform_cells.erase(cell)
		var cell_world = map_to_world(cell)
		if cell_world.y < _next_start:
			var cloud = load("res://Enemies/Cloud.tscn").instance()
			add_child(cloud)
			cloud.position.x= cell_world.x
			cloud.position.y = cell_world.y +128
			cloud_list.append(cloud)
			
func delete_obsolete_tiles():
	var obsolete = true
	var hazards = get_used_cells()
	var ground = plattforms.get_used_cells()
	for hazard in hazards:
		for plattform in ground:
			if hazard.y == plattform.y + 1:
				obsolete = false
	
		if obsolete == true:
			set_cell(hazard.x, hazard.y, -1)
	

