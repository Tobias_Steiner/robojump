extends "res://Tilemaps/TileMapCreator.gd"


####### Creating Level ############
var rng_x 
var rng_y 
var right_border
var upper_border
var right_border_steps
var upper_border_steps
var grid_cell_size = Vector2(64,64)
var block = 1
var next_start = 512

####################################


signal create_hazards
signal spawn_coins


func _ready():
	#####################################
	############ deleting Tiles ########
	add_to_group("delete_tiles_out_of_sight")
	#################################################
	

func _process(delta):
	if player.position.y - get_viewport_rect().size.y < next_start:
		_creating_level()
		
func delete_tiles(bottomline):   #Funktion wird von der Camera aufgerufen
	var bottomline_cell = world_to_map(Vector2(0,bottomline))
	for cell in get_used_cells():
		if cell.y > bottomline_cell.y:
			set_cell(cell.x, cell.y, -1)

func _get_random(nums):
	randomize()
	var idx = randi() % nums.size()
	return nums[idx]

func _set_right_and_upper_border():
	right_border = get_viewport_rect().size.x
	upper_border =  next_start - get_viewport_rect().size.y *2
	right_border_steps = range(0, right_border, grid_cell_size.x)
	upper_border_steps = range(next_start, upper_border, -grid_cell_size.y)
	next_start = _find_next_dividable(upper_border-128)



func _find_next_dividable(number) -> int:
	var l = floor(number)
	var i = -1
	while true:
		number = float(l + i) / float(grid_cell_size.y)
		if decimals(number) == 0:
			l += i
			return l - grid_cell_size.y*2
		i += -1
	return l



func _find_player_position_next_dividable() -> int:
	var player_position_dividable
	var l = floor(player.position.y)
	var i = -1
	while true:
		player_position_dividable = float(l + i) / float(grid_cell_size.y)
		if decimals(player_position_dividable) == 0:
			l += i
			return l - grid_cell_size.y*2
		i += -1
	return l

#func _delete_all_blocks_above_certain_height():
	#_set_right_and_upper_border()
	for cell in get_used_cells():
		var c = map_to_world(cell)
		if c.y < _find_player_position_next_dividable():
			set_cellv(cell,-1)

func checking_player_position_for_cell(cell : Vector2) -> bool: 
	var player_position = world_to_map(Vector2(0,_find_player_position_next_dividable()))
	if cell.y <player_position.y-1:
		return true
	else:
		return false

func _deleting_all_blocks_on_border(custom_leftborder:int, custom_right_border:int):
	for cell in get_used_cells():
		if checking_player_position_for_cell(cell):
			var left_border_x = 0
			var right_border_x = world_to_map(Vector2(right_border, 0))
			if cell.x < left_border_x + custom_leftborder :
				set_cell(cell.x,cell.y,-1)
			if cell.x >= right_border_x.x - custom_right_border:
				set_cell(cell.x,cell.y,-1)

func _clear_cells_in_area(bottom_line :int, upper_line :int, left_line :int, right_line :int):
	for cell in get_used_cells():
		if cell.x >= left_line && cell.x <= right_line && cell.y <= bottom_line && cell.y >= upper_line:
			set_cell(cell.x,cell.y, -1)

func _delete_all_Single_blocks():
	for cell in get_used_cells():
		var delete =true
		for cell_check in get_used_cells(): ##Hier wird jetzt gecheckt ob rund um den block noch ein anderer block ist wenn nicht wird der gelöscht
			if cell.y-1 == cell_check.y && cell.x == cell_check.x:
				delete=false
			if cell.y+1 == cell_check.y && cell.x == cell_check.x:
				delete=false
			if cell.y == cell_check.y && cell.x-1 == cell_check.x:
				delete=false
			if cell.y == cell_check.y && cell.x+1 == cell_check.x:
				delete=false
			if cell.y-1 == cell_check.y && cell.x+1 == cell_check.x:
				delete=false
			if cell.y-1 == cell_check.y && cell.x-1 == cell_check.x:
				delete=false
			if cell.y+1 == cell_check.y && cell.x+1 == cell_check.x:
				delete=false
			if cell.y+1 == cell_check.y && cell.x-1 ==cell_check.x:
				delete=false
		if delete:
			set_cell(cell.x,cell.y,-1)

func _creating_level():
	_set_right_and_upper_border()
	#_delete_all_blocks_above_certain_height()


	for i in 40:
		rng_x = _get_random(right_border_steps)
		rng_y = _get_random(upper_border_steps)
		var v = world_to_map(Vector2(rng_x,rng_y))
		set_cell(v.x,v.y, block)
	post_processing_placement()
	_deleting_all_blocks_on_border(2,2)
	_delete_all_Single_blocks()
	
	emit_signal("create_hazards", next_start +get_viewport_rect().size.y *2)
	emit_signal("spawn_coins")


func post_processing_placement():
	#var player_position = world_to_map(Vector2(0,_find_player_position_next_dividable())) # der 0 ist egal geht mir nur um die y position
	var check_y= [-1,-2,-3,-4,-5] # sind die felder die nach oben gecheckt werden
	var check_x= [0, 1, 2] # sind die felder die nach oben gecheckt werden
	for checking_cell in get_used_cells():
		for used_cell in get_used_cells():
			if checking_player_position_for_cell(used_cell):
				for y in check_y:
					for x in check_x:
						if checking_cell.y + y == used_cell.y && checking_cell.x + x == used_cell.x:
							set_cell(checking_cell.x, checking_cell.y, -1)
						if checking_cell.y + y == used_cell.y && checking_cell.x - x == used_cell.x:
							set_cell(checking_cell.x, checking_cell.y, -1)
	
	for checking_cell in get_used_cells():
		if checking_player_position_for_cell(checking_cell):
			var posibilities = _get_random(range(1,10))
			if posibilities <=8:
				_create_width(checking_cell)
			else:
				_create_jump_platforms(checking_cell)

func _create_width(cell: Vector2):
	var cell_width = _get_random(range(2,4))
	for i in cell_width:
		set_cell(cell.x + i,cell.y, block)

func _create_jump_platforms(cell:Vector2):
	var platforms = _get_random(range(2,4))
	var platform_width = _get_random(range(2,3))
	var platform_height =  _get_random(range(5,6))
	var cell_y = cell.y
	_clear_cells_in_area(cell.y, cell.y -platforms*platform_height, cell.x, cell.x +platform_width)
	for platform in platforms:
		for width in platform_width:
			set_cell(cell.x + width, cell_y, block)
		cell_y = cell_y - platform_height
func _create_stairs():
	pass


