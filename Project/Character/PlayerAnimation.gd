extends AnimatedSprite


func _on_Player_animate(motion):
	if motion.x > 0:
		play("walk")
		flip_h = false
	if motion.x < 0:
		play("walk")
		flip_h = true
	if motion.x == 0:
		play("idle")
		flip_h = false
	if motion.y < 0:
		play("jump")
