extends KinematicBody2D



onready var timer = get_node("Timer")


var motion = Vector2(0,0)
const SPEED = 700
const GRAVITY = 8000
const UP = Vector2(0, -1)
const JUMP_SPEED = 2500

#var lifes = 10
var player_pos_now = position
var jumps_in_air = 2
var jumps_in_air_count = 0
signal animate
signal playerposition
signal collided


func _physics_process(delta):
	_apply_gravity(delta)
	_jump()
	_move()
	move_and_slide(motion, UP)
	emit_signal("playerposition", position.y, $Camera2D.limit_bottom)
	if is_on_floor():
		jumps_in_air_count =0 
	
	for i in get_slide_count():
		var collision =get_slide_collision(i)
		if collision:
			if collision.collider.is_in_group("danger"):
				hurt()



func _apply_gravity(delta):
	if is_on_floor():
		motion.y = 0
	elif is_on_ceiling():
		motion.y = 50
	else:
		motion.y += GRAVITY * delta

func _jump():
	if Input.is_action_just_pressed("jump") and jumps_in_air_count < jumps_in_air:
		jumps_in_air_count += 1
		motion.y = -JUMP_SPEED
		emit_signal("animate", motion)
		


func _move():
	if Input.is_action_pressed("walk_left")and not Input.is_action_pressed("walk_right"):
		motion.x = -SPEED 
		emit_signal("animate", motion)
	elif Input.is_action_pressed("walk_right") and not Input.is_action_pressed("walk_left"):
		motion.x = SPEED
		emit_signal("animate", motion)
	else:
		motion.x = 0
		emit_signal("animate",motion)

func hurt():
	#if(timer.is_stopped() == true):
	#	test_loeschen_wenn_fertig +=1
	#	print(test_loeschen_wenn_fertig)
	#	position.y -= 150
	#timer.start()
	get_tree().call_group("gameMaster", "end_Game")
