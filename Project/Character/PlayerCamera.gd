extends Camera2D

var viewport_x = get_viewport_rect().size.x
var viewport_y = get_viewport_rect().size.y
var camera_screen = get_camera_screen_center()

onready var player= get_node("..")


func _ready():
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,  SceneTree.STRETCH_ASPECT_EXPAND, Vector2(1920, 1080))
	limit_left = 0
	limit_right = get_viewport_rect().size.x
	limit_bottom =  camera_screen.y + get_viewport_rect().size.y
	add_to_group("delete_tiles_out_of_sight")

func _process(_delta):
	if limit_right != get_viewport_rect().size.x:
		limit_right = get_viewport_rect().size.x
	if camera_screen != get_camera_screen_center() && camera_screen.y > get_camera_screen_center().y && player.is_on_floor():
		camera_screen = get_camera_screen_center()
		limit_bottom =  camera_screen.y + get_viewport_rect().size.y * 0.5
		get_tree().call_group("delete_tiles_out_of_sight", "delete_tiles", limit_bottom)

